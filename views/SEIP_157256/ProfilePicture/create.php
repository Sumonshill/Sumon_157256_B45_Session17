<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Birth Date Collection Form</title>
    <link rel="stylesheet" href="../../../src/BITM/SEIP_157256/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="main-body">
    <div class="header">
        <h1 href="#">PROFILE PICTURE</h1>
    </div>
    <div class="notification">
        <div class="message text-center">
            <h3><?php echo $msg;?></h3>
        </div>
       </div>
    <form action="store.php" method="post" class="text-center" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">Enter User Name</label>
            <input type="text" class="form-control text-center" name="name" placeholder="Type User Name Here">
        </div>
        <div class="form-group" id="na">
            <label for="picture">Select Your Picture</label>
            <input type="file" class="file" name="picture">
        </div>
        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
    </form>
</div>
<script src="../../../resource/bootstrap-3.3.7/js/jquery.min.js"></script>
<script src="../../../resource/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<script>
    jQuery(function($){
        $('.message').fadeOut(550);
        $('.message').fadeIn(550);
        $('.message').fadeOut(550);
        $('.message').fadeIn(550);
        $('.message').fadeOut(550);
        $('.message').fadeIn(550);
        $('.message').fadeOut(550);
    })
</script>
</body>
</html>