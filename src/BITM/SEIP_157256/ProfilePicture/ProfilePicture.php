<?php


namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $pictureName;
    private $pictureTemLocation;
    private $pictureType;
    private $pictureSize;

    public function setData($allPostData = null)
    {
        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }
        if (array_key_exists("name", $allPostData)) {
            $this->name = $allPostData['name'];
        }
    }

    public function setProfilePicture($allFileData = null)
    {

        $this->pictureName = $allFileData['picture']['name'];
        $this->pictureTemLocation = $allFileData['picture']['tmp_name'];
        $this->pictureType = $allFileData['picture']['type'];
        $this->pictureSize = $allFileData['picture']['size'];
    }

    public function storePicture()
    {
        move_uploaded_file($this->pictureTemLocation, "UploadedPicture/" . $this->pictureName);
    }

    public function store()
    {

        $arrayData = array
        (
        $this->name,
        $this->pictureName,
        $this->pictureTemLocation,
        $this->pictureType,
        $this->pictureSize
        );
    $query = 'INSERT INTO profilePicture (name, pictureName, tmpLoaction, type, size) VALUES (?,?,?,?,?)';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

    if ($result) {
        Message::setMessage("success! Data has been stored!.");
    } else {
        Message::setMessage("Failed! Data has been stored!.");
    }

    Utility::redirect('create.php');
  }

}