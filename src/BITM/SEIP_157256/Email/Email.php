<?php

namespace App\Email;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Email extends DB{
    private $id;
    private $userName;
    private $email;

    public function setData($allPostData = null)
    {
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("userName",$allPostData)){
            $this->userName = $allPostData['userName'];
        }
        if(array_key_exists("email",$allPostData)){
            $this->email = $allPostData['email'];
        }
    }

    public function store()
    {
        $arrayData = array($this->userName,$this->email);
        $query = 'INSERT INTO email (userName, email) VALUES (?,?)';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been stored!.");
        }
        else{
            Message::setMessage("Failed! Data has been stored!.");
        }

        Utility::redirect('create.php');
    }

}