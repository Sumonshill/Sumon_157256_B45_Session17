<?php

namespace App\Gender;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Gender extends DB{
    private $id;
    private $name;
    private $gender;

    public function setData($allPostData = null)
    {
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("gender",$allPostData)){
            $this->gender = $allPostData['gender'];
        }
    }

    public function store()
    {
        $arrayData = array($this->name, $this->gender);
        $query = 'INSERT INTO gender (name, gender) VALUES (?,?)';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if ($result) {
            Message::setMessage("Success! Data has been stored!.");
        } else {
            Message::setMessage("Failed! Data has been stored!.");
        }

        Utility::redirect('create.php');
    }
}